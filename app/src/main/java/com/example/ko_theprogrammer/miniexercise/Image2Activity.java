package com.example.ko_theprogrammer.miniexercise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.example.ko_theprogrammer.myapplication.R;

public class Image2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image2);
    }

    public void back(View view) {
        finish();
    }
}