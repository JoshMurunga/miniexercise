package com.example.ko_theprogrammer.miniexercise;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.example.ko_theprogrammer.myapplication.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void toImage1(View view) {
        Intent intent = new Intent(this, Image1Activity.class);
        startActivity(intent);
    }

    public void toImage2(View view) {
        Intent intent = new Intent(this, Image2Activity.class);
        startActivity(intent);
    }

    public void toImage3(View view) {
        Intent intent = new Intent(this, Image3Activity.class);
        startActivity(intent);
    }
}